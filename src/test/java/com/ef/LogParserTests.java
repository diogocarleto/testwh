package com.ef;

import com.ef.model.Log;
import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

/**
 * Created by @author diogocarleto on 14/12/18
 */
public class LogParserTests {

    @Test
    public void parseLine() {
        String line = "2017-01-01 00:00:11.763|192.168.234.82|\"GET / HTTP/1.1\"|200|\"swcd (unknown version) CFNetwork/808.2.16 Darwin/15.6.0\"";

        Log log = Log.parseLine(line);
        assertThat(log, notNullValue());

    }
}
