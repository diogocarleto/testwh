package com.ef;

import com.ef.exception.ErrorMessagesEnum;
import com.ef.exception.ParserException;
import com.ef.model.InputParameters;
import org.junit.Test;

import java.nio.file.Paths;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

/**
 * Created by @author diogocarleto on 14/12/18
 */
public class ParserTests {

    @Test
    public void parseInputParametersInvalidAccesslog() {
        String[] a = new String[] {
                "--startDate=2017-01-01.00:00:00",
                "--duration=daily",
                "--threshold=500",
                "--accesslog=/path/to/file"};

        try {
            InputParameters inputParameters = InputParameters.parse(a);
        } catch (ParserException ex) {
            assertThat(ex.getErrorMessagesEnum(), is(ErrorMessagesEnum.INVALID_ACCESSLOG));
        }
    }

    @Test
    public void parseInputParametersInvalidStartDate() throws Exception {
        String accesslogPath = Paths.get(ClassLoader.getSystemResource("access.log").toURI()).toString();
        String[] a = new String[] {
                "--startDate=2017555-01-01.00:00:00",
                "--duration=daily",
                "--threshold=500",
                "--accesslog="+accesslogPath};

        try {
            InputParameters inputParameters = InputParameters.parse(a);
        } catch (ParserException ex) {
            assertThat(ex.getErrorMessagesEnum(), is(ErrorMessagesEnum.INVALID_STARTDATE));
        }
    }

    @Test
    public void parseInputParametersInvalidDuration() throws Exception {
        String accesslogPath = Paths.get(ClassLoader.getSystemResource("access.log").toURI()).toString();
        String[] a = new String[] {
                "--startDate=2017-01-01.00:00:00",
                "--duration=WEEKLY",
                "--threshold=500",
                "--accesslog="+accesslogPath};

        try {
            InputParameters inputParameters = InputParameters.parse(a);
        } catch (ParserException ex) {
            assertThat(ex.getErrorMessagesEnum(), is(ErrorMessagesEnum.INVALID_DURATION));
        }
    }

    @Test
    public void parseInputParametersInvalidThreshold() throws Exception {
        String accesslogPath = Paths.get(ClassLoader.getSystemResource("access.log").toURI()).toString();
        String[] a = new String[] {
                "--startDate=2017-01-01.00:00:00",
                "--duration=daily",
                "--threshold=abd",
                "--accesslog="+accesslogPath};

        try {
            InputParameters inputParameters = InputParameters.parse(a);
        } catch (ParserException ex) {
            assertThat(ex.getErrorMessagesEnum(), is(ErrorMessagesEnum.INVALID_THRESHOLD));
        }
    }

    @Test
    public void parseInsufficientInputParameters() throws Exception {
        String accesslogPath = Paths.get(ClassLoader.getSystemResource("access.log").toURI()).toString();
        String[] a = new String[] {
                "--startDate=2017-01-01.00:00:00",
                "--duration=daily",
                "--accesslog="+accesslogPath};

        try {
            InputParameters inputParameters = InputParameters.parse(a);
        } catch (ParserException ex) {
            assertThat(ex.getErrorMessagesEnum(), is(ErrorMessagesEnum.INVALID_PARAMETER));
        }
    }

    @Test
    public void parseInputParameters() throws Exception {
        String accesslogPath = Paths.get(ClassLoader.getSystemResource("access.log").toURI()).toString();
        String[] a = new String[] {
                "--startDate=2017-01-01.00:00:00",
                "--duration=daily",
                "--threshold=500",
                "--accesslog="+accesslogPath};

        InputParameters inputParameters = InputParameters.parse(a);
        assertThat(inputParameters, notNullValue());
        assertThat(inputParameters.getAccesslog(), is(accesslogPath));
        assertThat(inputParameters.getDurationEnum(), is(InputParameters.DurationEnum.DAILY));
        assertThat(inputParameters.getThreshold(), is(500));
    }
}
