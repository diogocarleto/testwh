package com.ef.model;

import com.ef.exception.ErrorMessagesEnum;
import com.ef.exception.ParserException;

import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * Created by @author diogocarleto on 14/12/18
 */
public class InputParameters {

    private final String accesslog;
    private final LocalDateTime startDate;
    private final DurationEnum durationEnum;
    private final Integer threshold;

    private InputParameters(String accesslog, LocalDateTime startDate, DurationEnum durationEnum, Integer threshold) {
        this.accesslog = accesslog;
        this.startDate = startDate;
        this.durationEnum = durationEnum;
        this.threshold = threshold;
    }

    public static InputParameters parse(String[] args) {
        String accesslog = findParameter("accesslog", args);
        String startDate = findParameter("startDate", args);
        String duration = findParameter("duration", args);
        String threshold = findParameter("threshold", args);

        checkPathAccesslogValid(accesslog);
        return new InputParameters(
                accesslog,
                parseStartDate(startDate),
                DurationEnum.getForValue(duration),
                parseThreshold(threshold));
    }

    private static String findParameter(String key, String[] args) {
        String keyFormatted = "--"+key+"=";
        Stream<String> streamArgs = Arrays.asList(args).stream();
        Optional<String> valueOpt = streamArgs.filter(s -> s.contains(keyFormatted))
                .findFirst();
        if (valueOpt.isPresent()) {
            return valueOpt.get().replace(keyFormatted, "");
        }

        throw new ParserException(ErrorMessagesEnum.INVALID_PARAMETER);
    }

    private static LocalDateTime parseStartDate(String startDate) {
        try {
            return LocalDateTime.parse(startDate, DateTimeFormatter.ofPattern("yyyy-MM-dd.HH:mm:ss"));
        } catch (RuntimeException ex) {
            throw new ParserException(ErrorMessagesEnum.INVALID_STARTDATE);
        }
    }

    private static Integer parseDuration(String duration) {
        try {
            return Integer.valueOf(duration);
        } catch (Exception ex) {
            throw new ParserException(ErrorMessagesEnum.INVALID_DURATION);
        }
    }

    private static Integer parseThreshold(String duration) {
        try {
            return Integer.valueOf(duration);
        } catch (Exception ex) {
            throw new ParserException(ErrorMessagesEnum.INVALID_THRESHOLD);
        }
    }

    private static void checkPathAccesslogValid(String accesslog) {
        try {
            Paths.get(accesslog).toFile();
        } catch (Exception ex) {
            throw new ParserException(ErrorMessagesEnum.INVALID_ACCESSLOG);
        }
    }

    public LocalDateTime getEndDate() {
        if (durationEnum == null) {
            throw new IllegalArgumentException("duration not defined");
        }

        switch (durationEnum) {
            case DAILY: return startDate.plusHours(24);
            case HOURLY: return startDate.plusHours(1);
            default: return null;
        }
    }

    public String getAccesslog() {
        return accesslog;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public DurationEnum getDurationEnum() {
        return durationEnum;
    }

    public Integer getThreshold() {
        return threshold;
    }

    public enum DurationEnum {
        HOURLY("hourly"),
        DAILY("daily");

        final String value;

        DurationEnum(String value) {
            this.value = value;
        }

        public static DurationEnum getForValue(String value) {
            Optional<DurationEnum> durationEnumOpt = Arrays.stream(DurationEnum.values())
                    .filter(d -> d.value.equalsIgnoreCase(value))
                    .findFirst();
            if (!durationEnumOpt.isPresent()) {
                throw new ParserException(ErrorMessagesEnum.INVALID_DURATION);
            }
            return durationEnumOpt.get();
        }
    }
}




