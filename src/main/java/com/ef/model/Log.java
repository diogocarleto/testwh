package com.ef.model;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by @author diogocarleto on 13/12/18
 */
@NamedNativeQueries({
        @NamedNativeQuery(name = Log.NQ_FIND_BLOCKED_IPS,
        query = "select distinct ip from Log " +
                "where date between :startDate and :endDate " +
                "group by ip " +
                "having count(ip) > :threshold ")

})
@Entity
public class Log extends BaseEntity {

    public static final String NQ_FIND_BLOCKED_IPS = "Log.findBlockedIps";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private LocalDateTime date;
    private String ip;
    private String request;
    private String status;
    private String userAgent;

    public Log() {
    }

    public Log(LocalDateTime date, String ip, String request, String status, String userAgent) {
        this.date = date;
        this.ip = ip;
        this.request = request;
        this.status = status;
        this.userAgent = userAgent;
    }

    public static Log parseLine(String line) {
        String[] fields = line.split("\\|");

        return new Log(
                LocalDateTime.parse(fields[0], DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS")),
                fields[1],
                fields[2],
                fields[3],
                fields[4]);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }
}
