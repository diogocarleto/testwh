package com.ef.service;

import com.ef.infra.JPAUtils;
import com.ef.model.BlockedIp;
import com.ef.model.InputParameters;
import com.ef.model.Log;

import javax.persistence.NoResultException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/**
 * Created by @author diogocarleto on 14/12/18
 */
public class LogService {

    public void parseAndSaveFile(String accesslog) {
        try {
            List<String> subLines = new ArrayList<>();

            Stream<String> lines = Files.lines(Paths.get(accesslog));
            lines.forEach(line -> {
                if (subLines.size()==1000) {
                    saveLog(subLines);
                    subLines.clear();
                }
                subLines.add(line);
            });

            saveLog(subLines);
        } catch (IOException ex) {
            //TODO handle this exception
        }
    }

    private void saveLog(List<String> lines) {
        List<Log> logs = new ArrayList<>();
        lines.forEach(line -> logs.add(Log.parseLine(line)));
        JPAUtils.save(logs);
    }

    public List<String> findBlockedIps(InputParameters inputParameters) {
        try {
            List<String> blockedIps = JPAUtils.getEntityManager().createNamedQuery(Log.NQ_FIND_BLOCKED_IPS)
                    .setParameter("startDate", inputParameters.getStartDate())
                    .setParameter("endDate", inputParameters.getEndDate())
                    .setParameter("threshold", inputParameters.getThreshold())
                    .getResultList();

            saveBlockedIps(blockedIps, inputParameters);
            return blockedIps;
        } catch (NoResultException ex) {
            return new ArrayList<>();
        }
    }

    private void saveBlockedIps(List<String> blockedIps, InputParameters inputParameters) {
        StringBuilder comments = new StringBuilder("more than ")
                .append(inputParameters.getThreshold())
                .append(" requests between ")
                .append(inputParameters.getStartDate())
                .append(" and ")
                .append(inputParameters.getEndDate());

        blockedIps.forEach(ip -> JPAUtils.save(new BlockedIp(ip, comments.toString())));
    }

}
