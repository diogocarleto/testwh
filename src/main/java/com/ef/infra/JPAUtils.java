package com.ef.infra;

import com.ef.model.BaseEntity;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.util.List;

/**
 * Created by @author diogocarleto on 14/12/18
 */
public final class JPAUtils {

    private static EntityManager entityManager;

    private JPAUtils() {
    }

    public static EntityManager getEntityManager() {
        if (entityManager == null || !entityManager.isOpen()) {
            entityManager = Persistence.createEntityManagerFactory( "pu" ).createEntityManager();
        }

        return entityManager;
    }

    public static <T extends BaseEntity> T save(T entity) {
        EntityManager entityManager = getEntityManager();
        entityManager.getTransaction().begin();
        entityManager.persist(entity);
        entityManager.getTransaction().commit();
        return entity;
    }

    public static <T extends BaseEntity> List<T> save(List<T> entities) {
        EntityManager entityManager = getEntityManager();
        entityManager.getTransaction().begin();

        for (int i=0; i < entities.size(); i++) {
            entityManager.persist(entities.get(i));
            if ( i % 50 == 0 ) {
                entityManager.flush();
                entityManager.clear();
            }
        }
        entityManager.getTransaction().commit();
        return entities;
    }
}
