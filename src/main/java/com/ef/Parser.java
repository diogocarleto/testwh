package com.ef;

import com.ef.model.InputParameters;
import com.ef.service.LogService;

/**
 * Hello world!
 *
 */
public class Parser {
    public static void main( String[] args ) {
        InputParameters inputParameters = InputParameters.parse(args);
        LogService logService = new LogService();
        logService.parseAndSaveFile(inputParameters.getAccesslog());
        System.out.println(logService.findBlockedIps(inputParameters));
    }
}
