package com.ef.exception;

import java.text.MessageFormat;

/**
 * Created by @author diogocarleto on 14/12/18
 */
public enum ErrorMessagesEnum {

    INVALID_ACCESSLOG("Invalid accesslog"),
    INVALID_STARTDATE("Invalid startDate"),
    INVALID_DURATION("Invalid duration"),
    INVALID_THRESHOLD("Invalid threshold"),
    INVALID_PARAMETER("Invalid parameter: {0}");

    public final String message;

    ErrorMessagesEnum(String message) {
        this.message = message;
    }
}
