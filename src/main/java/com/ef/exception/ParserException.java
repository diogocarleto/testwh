package com.ef.exception;

import java.text.MessageFormat;

/**
 * Created by @author diogocarleto on 14/12/18
 */
public class ParserException extends RuntimeException {

    private final Object[] params;
    private final ErrorMessagesEnum errorMessagesEnum;

    public ParserException(ErrorMessagesEnum errorMessagesEnum, Object... params) {
        this.params = params;
        this.errorMessagesEnum = errorMessagesEnum;
    }

    public String getMessageFormmated(String... params) {
        return MessageFormat.format(errorMessagesEnum.message, params);
    }

    public Object[] getParams() {
        return params;
    }

    public ErrorMessagesEnum getErrorMessagesEnum() {
        return errorMessagesEnum;
    }
}
