create table Log(
  id        bigint auto_increment primary key,
  date      datetime(6)  not null,
  ip        varchar(255) not null,
  request   varchar(255) not null,
  status    varchar(255) null,
  userAgent varchar(255) null
);

create table BlockedIp (
  id       bigint auto_increment primary key,
  comments varchar(255) not null,
  ip       varchar(255) not null
);

